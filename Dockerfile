FROM openjdk:8u171-alpine3.7
RUN apk --no-cache add curl
COPY target/microk8*.jar microk8.jar
CMD java ${JAVA_OPTS} -jar microk8.jar