package it.coopservice.controller;

import io.micronaut.http.annotation.Controller;
import it.coopservice.api.service.AbstractController;
import it.coopservice.model.SchematronSupported;
import it.coopservice.repository.SchematronSupportedSLRepository;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


@Controller("/schematrons")
public class SchematronSupportedController extends AbstractController<SchematronSupported> {

    @Inject
    SchematronSupportedSLRepository schematronSupportedRepository;
    

    @PostConstruct
    public void init() {
        this.repository = schematronSupportedRepository;
    }

}
