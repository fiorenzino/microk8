package it.coopservice.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import it.coopservice.repository.SimpleRepository;

import javax.inject.Inject;

@Controller("/prova")
public class ProvaController {

    @Inject
    SimpleRepository simpleRepository;



    @Get("/")
    public String index() {
        return "" + simpleRepository.getAllSize();
    }
}