package it.coopservice.api.service;

import io.micronaut.http.HttpParameters;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import it.coopservice.api.repository.AbstractRepository;
import it.coopservice.api.repository.Repository;
import it.coopservice.api.repository.Search;
import it.coopservice.api.util.RepositoryUtils;
import org.hibernate.engine.spi.SessionImplementor;
import org.jboss.logging.Logger;

import javax.annotation.Nullable;
import javax.persistence.NoResultException;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.micronaut.http.HttpStatus.*;

public abstract class AbstractController<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected final Logger logger = Logger.getLogger(getClass());

    protected Repository<T> repository;

    public AbstractController() {

    }

    protected T unproxy(T object) {
        try {
            return (T) ((AbstractRepository<T>) getRepository()).getEm().unwrap(SessionImplementor.class).getPersistenceContext().unproxy(object);
        } catch (Exception e) {

        }
        return object;
    }
    
    /*
     * C
     */

    protected void prePersist(T object) throws Exception {
    }

    @Post
    public HttpResponse persist(T object) throws Exception {
        try {
            prePersist(object);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return HttpResponse
                    .status(BAD_REQUEST)
                    .body("Error before creating resource: " + e.getMessage());
        }
        try {
            T persisted = doPersist(object);

            if (persisted == null || getId(persisted) == null) {
                logger.error("Failed to create resource: " + object);
                return HttpResponse.status(INTERNAL_SERVER_ERROR)
                        .body("Failed to create resource: " + object);
            } else {
                return HttpResponse.status(OK).body(unproxy(object));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error creating resource: " + object);
        } finally {
            try {
                postPersist(object);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    protected T doPersist(T object) throws Exception {
        return repository.persist(object);
    }

    protected void postPersist(T object) throws Exception {
    }

    /*
     * R
     */

    @Get("/{id}")
    public HttpResponse fetch(String id) {
        logger.info("@GET :" + id);
        try {
            T t = repository.fetch(repository.castId(id));
            postFecth(t);
            if (t == null) {
                return HttpResponse.status(NOT_FOUND)
                        .body("Resource not found for ID: " + id);
            } else {
                return HttpResponse.status(OK).body(unproxy(t));
            }
        } catch (NoResultException e) {
            logger.error(e.getMessage());
            return HttpResponse.status(NOT_FOUND)
                    .body("Resource not found for ID: " + id);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error reading resource for ID: " + id);
        }
    }

    protected void postFecth(T object) {
    }

    /*
     * U
     */

    protected void preUpdate(T object) throws Exception {
    }

    @Put("/{id}")
    public HttpResponse update(String id, @Body T object) throws Exception {
        logger.info("@PUT update:" + object.toString());
        try {
            preUpdate(object);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse
                    .status(BAD_REQUEST)
                    .body("Errore before updating resource: "
                            + e.getMessage());
        }
        try {
            repository.update(object);
            return HttpResponse.status(OK).body(unproxy(object));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error updating resource: " + object);
        } finally {
            try {
                postUpdate(object);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    protected void postUpdate(T object) throws Exception {
    }

    /*
     * D
     */

    @Delete("/{id}")
    public HttpResponse delete(String id) throws Exception {
        logger.info("@DELETE:" + id);
        T object = null;
        try {
            object = repository.fetch(repository.castId(id));
            preDelete(object);
            repository.delete(repository.castId(id));
            return HttpResponse.status(NO_CONTENT)
                    .body("Resource deleted for ID: " + id);
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(NOT_FOUND)
                    .body("Resource not found for ID: " + id);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error deleting resource for ID: " + id);
        } finally {
            try {
                postDelete(object);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    protected void preDelete(T object) throws Exception {

    }

    protected void postDelete(T object) throws Exception {
    }

    /*
     * E
     */

    @Get("/{id}/exist")
    public HttpResponse exist(String id) {
        logger.info("@GET exist:" + id);
        try {
            boolean exist = repository.exist(repository.castId(id));
            if (!exist) {
                return HttpResponse.status(NOT_FOUND)
                        .body("Resource not found for ID: " + id);
            } else {
                return HttpResponse.status(FOUND)
                        .body("Resource exists for ID: " + id);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error reading resource for ID: " + id);
        }
    }

    /*
     * Q
     */

    @Get
    public HttpResponse getList(
            @QueryValue(value = "startRow", defaultValue = "0") Integer startRow,
            @QueryValue(value = "pageSize", defaultValue = "10") Integer pageSize,
            @Nullable @QueryValue("orderBy") String orderBy,
            HttpRequest<?> ui) {

//        logger.info("@GET list");
        try {
            Search<T> search = getSearch(ui, orderBy);
            int listSize = repository.getListSize(search);
            List<T> list = repository.getList(search, startRow, pageSize);
            List<T> ok = list.parallelStream().map(ent -> unproxy(ent)
//            {return unproxy(ent);}
            ).collect(Collectors.toList());

            postList(ok);
            // PaginatedListWrapper<T> wrapper = new PaginatedListWrapper<>();
            // wrapper.setList(list);
            // wrapper.setListSize(listSize);
            // wrapper.setStartRow(startRow);
            return HttpResponse.status(OK).body(list)
                    .header("Access-Control-Expose-Headers", "startRow, pageSize, listSize, startRow")
                    .header("startRow", startRow.toString())
                    .header("pageSize", pageSize.toString())
                    .header("listSize", "" + listSize)
                    .header("startRow", startRow.toString());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return HttpResponse.status(INTERNAL_SERVER_ERROR)
                    .body("Error reading resource list");
        }
    }

    protected void postList(List<T> list) {
    }

    protected Search<T> getSearch(HttpRequest<?> ui, String orderBy) {
        Search<T> s = new Search<T>(getClassType());
        if (orderBy != null && !orderBy.trim().isEmpty()) {
            s.setOrder(orderBy);
        }
        if (ui != null && ui.getParameters() != null
                && !ui.getParameters().isEmpty()) {
            HttpParameters queryParams = ui.getParameters();
            // TODO - DA TESTARE:
            // tutte le prop su search.getObj() possono essere in "chiaro" - senza prefissi
            // tutte le prop su oggetti search.getFrom() - search.getTo() sono con prefisso: es.from.id - from.dataInit

            makeSearch(queryParams, s);

        }

        return s;
    }

    <T> void makeSearch(HttpParameters queryParams, Search<T> s) {
        for (String key : queryParams.names()) {
            try {
                T instance = s.getObj();
                Optional<String> value = queryParams.getFirst(key);

                String fieldName = key;
                if (key.startsWith("obj.")) {
                    instance = s.getObj();
                    fieldName = key.substring(4);
                } else if (key.startsWith("from.")) {
                    instance = s.getFrom();
                    fieldName = key.substring(5);
                } else if (key.startsWith("to.")) {
                    instance = s.getTo();
                    fieldName = key.substring(3);
                } else if (key.startsWith("like.")) {
                    instance = s.getLike();
                    fieldName = key.substring(5);
                } else if (key.startsWith("not.")) {
                    instance = s.getNot();
                    fieldName = key.substring(4);
                }
                RepositoryUtils.setFieldByName(instance.getClass(), instance, fieldName, value.get());

            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

    }

    protected Repository<T> getRepository() {
        return repository;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private Class<T> getClassType() {
        Class clazz = getClass();
        while (!(clazz.getGenericSuperclass() instanceof ParameterizedType)) {
            clazz = clazz.getSuperclass();
        }
        ParameterizedType parameterizedType = (ParameterizedType) clazz
                .getGenericSuperclass();
        return (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    /**
     * Override this is needed
     *
     * @param t
     * @return
     */

    protected Object getId(T t) {
        return RepositoryUtils.getId(t);
    }

    @Options
    public HttpResponse options() {
        logger.info("@OPTIONS");
        return HttpResponse.ok();
    }

    public static HttpResponse jsonResponse(Map<String, String> toJson, HttpStatus status) {
        return HttpResponse.status(status)
                .body(toJson);
    }

    public static HttpResponse jsonResponse(HttpStatus status, String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value == null ? "(null)" : value.toString());
        return HttpResponse.status(status).body(map);
    }

}
