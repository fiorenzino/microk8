package it.coopservice.api.repository;

import io.micronaut.spring.tx.annotation.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseStatelessRepository<T> extends AbstractStatelessRepository<T> {

    private static final long serialVersionUID = 1L;

    @PersistenceContext
    EntityManager entityManager;
    StatelessSession em;

//    @PostConstruct
//    @Transactional
//    public void post() {
//        Session session = entityManager.unwrap(Session.class);
//        SessionFactory sessionFactory = session.getSessionFactory();
//        em = sessionFactory.openStatelessSession();
//    }


    @Override
    @Transactional
    public StatelessSession getEm() {
        if (em == null) {
            Session session = entityManager.unwrap(Session.class);
            SessionFactory sessionFactory = session.getSessionFactory();
            em = sessionFactory.openStatelessSession();
        }
        return em;
    }

    @Override
    public void setEm(StatelessSession em) {
        this.em = em;
    }

    @Override
    protected String getDefaultOrderBy() {
        return "id";
    }

}
