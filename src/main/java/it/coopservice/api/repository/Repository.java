package it.coopservice.api.repository;

import java.util.List;

/**
 * @param <T>
 */
public interface Repository<T> {

    /**
     * @param search
     * @param startRow
     * @param pageSize
     * @return
     */
    List<T> getList(Search<T> search, int startRow, int pageSize)
            throws Exception;

    /**
     * @param search
     * @return
     */
    int getListSize(Search<T> search) throws Exception;

    /**
     * Find by primary key
     *
     * @param key
     * @return
     */
    T find(Object key) throws Exception;

    /**
     * Fetch by primary key
     *
     * @param key
     * @return
     */
    T fetch(Object key) throws Exception;

    /**
     * create
     *
     * @param domainClass
     * @return
     */
    T create(Class<T> domainClass) throws Exception;

    /**
     * Make an instance persistent.
     * <p/>
     *
     * @param object
     * @return
     */
    T persist(T object) throws Exception;

    /**
     * @param object
     * @return
     */
    T update(T object) throws Exception;

    /**
     * @param key
     * @return
     */
    void delete(Object key) throws Exception;

    /**
     * @param key
     * @return boolean
     */
    boolean exist(Object key) throws Exception;

    /**
     * @param key
     * @return deserialization of key to actual class of the key field
     */
    Object castId(String key) throws Exception;


}
