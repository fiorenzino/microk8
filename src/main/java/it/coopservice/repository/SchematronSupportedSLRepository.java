package it.coopservice.repository;

import it.coopservice.api.repository.BaseStatelessRepository;
import it.coopservice.api.repository.Search;
import it.coopservice.model.SchematronSupported;

import javax.inject.Singleton;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Created by fiorenzo on 25/03/16.
 */
@Singleton
public class SchematronSupportedSLRepository extends BaseStatelessRepository<SchematronSupported> {

    private static final long serialVersionUID = 1L;

    @Override
    protected String getDefaultOrderBy() {
        return "name asc";
    }

    @Override
    protected void applyRestrictions(Search<SchematronSupported> search, String alias,
                                     String separator, StringBuffer sb, Map<String, Object> params)
            throws Exception {
        // UUID
        if (search.getObj().getUuid() != null
                && !search.getObj().getUuid().isEmpty()) {
            sb.append(separator).append(alias)
                    .append(".uuid = :UUID ");
            params.put("UUID", search.getObj().getUuid());
            separator = " and ";
        }

        // SENDER_VAT
        if (search.getObj().getCustomerVat() != null
                && !search.getObj().getCustomerVat().isEmpty()) {

            // OR SENDER_VAT
            if (search.getOr().getCustomerVat() != null
                    && !search.getOr().getCustomerVat().isEmpty()) {
                sb.append(separator).append(" (").append(alias)
                        .append(".customerVat = :CUSTOMER_VAT1 ");
                params.put("CUSTOMER_VAT1", search.getObj().getCustomerVat());
                separator = " or ";
                sb.append(separator).append(alias)
                        .append(".customerVat = :CUSTOMER_VAT2 ").append(")");
                params.put("CUSTOMER_VAT2", search.getObj().getCustomerVat());

            } else {
                params.put("CUSTOMER_VAT", search.getObj().getCustomerVat());
            }
            separator = " and ";

        }

        // SENDER NAME
        if (search.getLike().getName() != null
                && !search.getLike().getName().isEmpty()) {
            sb.append(separator).append(" upper ( ").append(alias)
                    .append(".name ) like :NAME ");
            params.put("NAME", likeParam(search.getLike()
                    .getName().trim().toUpperCase()));
            separator = " and ";
        }
    }

    public List<String> findByName(String name, String customerVat) {
        List<String> schematrons = (List<String>) getEm()
                .createSQLQuery("select s.path from " + SchematronSupported.TABLE_NAME +
                        " s WHERE s.name = :NAME AND " +
                        "( s.customervat LIKE :CUSTOMER_VAT OR s.customervat = :ALL_CUSTVAT)")
                .setParameter("NAME", name)
                .setParameter("CUSTOMER_VAT", likeParam(customerVat))
                .setParameter("ALL_CUSTVAT", "*")
                .list();
        if (schematrons != null && schematrons.size() > 0)
            return schematrons;
        return null;
    }

    public Integer getAllSize() {
        BigInteger all = (BigInteger) getEm().createSQLQuery("select count(*) from "
                + SchematronSupported.TABLE_NAME).uniqueResult();
        return all.intValue();
    }

}
