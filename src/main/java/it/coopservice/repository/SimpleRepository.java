package it.coopservice.repository;

import io.micronaut.spring.tx.annotation.Transactional;
import it.coopservice.model.SchematronSupported;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;

@Singleton
public class SimpleRepository {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public Integer getAllSize() {
        BigInteger all = (BigInteger) em.createNativeQuery("select count(*) from "
                + SchematronSupported.TABLE_NAME).getSingleResult();
        return all.intValue();
    }
}
