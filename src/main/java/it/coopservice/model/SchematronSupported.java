package it.coopservice.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by fiorenzo on 25/03/16.
 */
@Entity
@Table(name = SchematronSupported.TABLE_NAME)
public class SchematronSupported implements Serializable
{
   private static final long serialVersionUID = -4581521841453347801L;

   public static final String TABLE_NAME = "schematronssupported";

   private String uuid;
   private String customerVat;
   private String name;
   private String path;

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(name = "uuid", unique = true)
   public String getUuid()
   {
      return uuid;
   }

   public void setUuid(String uuid)
   {
      this.uuid = uuid;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   @Column(name = "customervat")
   public String getCustomerVat()
   {
      return customerVat;
   }

   public void setCustomerVat(String customerVat)
   {
      this.customerVat = customerVat;
   }

   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }
}